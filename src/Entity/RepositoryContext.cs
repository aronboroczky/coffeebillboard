using Entity.Models;
using Microsoft.EntityFrameworkCore;

namespace Contracts
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            :base(options)
        {
            
        }

        public DbSet<Coffee> Coffees { get; set; }
    }
}