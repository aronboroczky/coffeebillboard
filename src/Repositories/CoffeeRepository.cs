using System.Collections.Generic;
using Contracts;
using Entity;
using Entity.Models;
using System.Linq;

namespace Repositories
{
    public class CoffeeRepository : Repository<Coffee>, ICoffeeRepository
    {
        public CoffeeRepository(RepositoryContext repositoryContext)
            :base(repositoryContext)
        {
            
        }

        public IEnumerable<Coffee> GeCoffeesPerPage(int pageIndex, int pageSize)
        {
            return RepositoryContext.Coffees
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }
    }
}