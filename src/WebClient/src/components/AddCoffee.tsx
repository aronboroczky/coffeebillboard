import * as React from 'react';
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { Button, Label, Form, Input, FormText, FormGroup} from 'reactstrap';

import * as CoffeesStore from '../store/Coffees';

type CoffeeProps =
    CoffeesStore.CoffeesState
    & typeof CoffeesStore.actionCreators

class AddCoffee extends React.PureComponent<CoffeeProps> {

    public render() {
        return (
            <React.Fragment>
                <h1 id="coffeeFormLabel">Now we can also add coffee</h1>
                <p>....over complications....</p>
                {this.renderCoffeeCreateForm()}
            </React.Fragment>
        );
    }

    private renderCoffeeCreateForm() {
        return (
            <Form>
                <FormGroup>
                    <Label for="coffeeTitle">Title</Label>
                    <Input type="text" name="coffeeTitle" id="coffeeTitle" placeholder="Coffee title" />
                </FormGroup>
                <FormGroup>
                    <Label for="coffeePrice">Price</Label>
                    <Input type="text" name="coffeePrice" id="coffeePrice" placeholder="Coffee price" />
                </FormGroup>
                <FormGroup>
                    <Label for="coffeeAvatar">Picture</Label>
                    <Input file="text" name="coffeeAvatar" id="coffeeAvatar" />
                    <FormText color="muted">
                        Upload a picture for the coffee.
                    </FormText>
                </FormGroup>
                <Button>Add Coffee</Button>
            </Form>
        )
    }
}

export default connect(
    (state: ApplicationState) => state.coffees, // Selects which state properties are merged into the component's props
    CoffeesStore.actionCreators // Selects which action creators are merged into the component's props
)(AddCoffee as any);