using System;

namespace Contracts
{
    public interface IRepositoryWrapper : IDisposable
    {
        ICoffeeRepository Coffees { get; }
        int Complete();

    }
}