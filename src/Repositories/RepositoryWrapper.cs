using Contracts;

namespace Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private readonly RepositoryContext _context;

        public RepositoryWrapper(RepositoryContext context)
        {
            _context = context;
        }

        private ICoffeeRepository _coffees;
        public ICoffeeRepository Coffees
        {
            get
            {
                if (_coffees == null)
                {
                    _coffees = new CoffeeRepository(_context);
                }
                return _coffees;
            }
        }
        
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}