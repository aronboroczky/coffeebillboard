using Contracts;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        public RepositoryContext RepositoryContext { get; set; }

        public Repository(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }


        public void Add(TEntity entity)
        {
            RepositoryContext.Set<TEntity>().Add(entity);
        }
        
        public void AddRange(IEnumerable<TEntity> entities)
        {
            RepositoryContext.Set<TEntity>().AddRange(entities);
        }

        public TEntity Get(int id)
        {
            return RepositoryContext.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return RepositoryContext.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return RepositoryContext.Set<TEntity>().Where(predicate);
        }

        public void Remove(TEntity entity)
        {
            RepositoryContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            RepositoryContext.Set<TEntity>().RemoveRange(entities);
        }


    }
}