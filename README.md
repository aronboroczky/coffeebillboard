# Coffee Billboard

The Coffee Billboard is a website to display, add or remove coffees.
With .net core backend and React frontend.

### requirements:
-   docker-compose
-   npm

## Start backend

Clone or download the repository and in the root folder run:
```sh
$ docker-compose up -d
```

## Start the web client
In the src/WebClient folder run:
```sh
$ npm start
```

## API

Postman: https://www.getpostman.com/collections/10ffe5a44d66bb92fa85

### GET /api/coffeebillboard -> response:
`[{"coffeeId":1,"title":"coffee_4","price":123,"avatarLocation":"coffee_4.png"}
{"coffeeId":2,"title":"coffee_4","price":123,"avatarLocation":"coffee_4.png"}
{"coffeeId":3,"title":"coffee_4","price":123,"avatarLocation":"coffee_4.png"}
{"coffeeId":4,"title":"coffee_4","price":123,"avatarLocation":"coffee_4.png"}]`

### POST /api/coffeebillboard -> body:
`{
    "Title":"coffee_14",
    "Price":12
}`

### DELETE /api/coffeebillboard/{id}:

