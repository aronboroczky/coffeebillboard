import { Action, Reducer } from 'redux';
import { AppThunkAction } from '.';

export interface CoffeesState {
    isLoading: boolean;
    page: number;
    coffees: Coffee[];
    errorMessage: string;
}

export interface Coffee {
    coffeeId: number;
    title: string;
    price: number;
    avatarLocation: string;
}

interface GetCoffeeListAction {
    type: 'GET_COFFEE_LIST';
    page: number;
}

interface AddCoffeeAction {
    type: 'ADD_COFFEE'
}

interface DeleteCoffeeAction {
    type: 'DELETE_COFFEE'
}

interface CoffeeListLoading {
    type: 'COFFEE_LIST_LOADING';
}

interface CoffeeListSuccess {
    type: 'COFFEE_LIST_SUCCESS';
    page: number;
    data: Coffee[];
}

interface CoffeeListFailed {
    type: 'COFFEE_LIST_FAILED';
    errorMessage: string;
}

type KnownAction = GetCoffeeListAction | AddCoffeeAction | DeleteCoffeeAction | CoffeeListLoading | CoffeeListSuccess | CoffeeListFailed;

export const actionCreators = {
    requestCoffees: (page: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.coffees && appState.coffees.page !== page) {
            fetch(`api/api/coffeebillboardtest/${page}`)
                .then(response => response.json() as Promise<Coffee[]>)
                .then(data => {
                    dispatch({ type: 'COFFEE_LIST_SUCCESS', data: data, page: page });
                });
                
            dispatch({ type: 'GET_COFFEE_LIST', page: 0 });
        }
    },

    deleteCoffee: (coffeeIdToDelete: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();

        if (appState && appState.coffees && coffeeIdToDelete) {
            fetch(`api/api/coffeebillboardtest/${coffeeIdToDelete}`, {
                method: "DELETE"
            })
            .then(response => response.json() as Promise<Coffee[]>)
            .then(data => {
                dispatch({ type: 'COFFEE_LIST_SUCCESS', data: data, page: 0 });
            });

            dispatch({ type: 'GET_COFFEE_LIST', page: 0 });
		}
	}
};


const unloadedState: CoffeesState = { coffees: [], isLoading: false, errorMessage: "", page: -1};


export const reducer: Reducer<CoffeesState> = (state: CoffeesState | undefined, incomingAction: Action): CoffeesState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'COFFEE_LIST_LOADING':
            return {
                ...state,
                isLoading: true
            };
        case 'COFFEE_LIST_SUCCESS':
            return {
                ...state,
                isLoading: false,
                page: action.page,
                coffees: action.data,
                errorMessage: ""
            }
        case 'COFFEE_LIST_FAILED':
            return {
                ...state,
                isLoading: false,
                errorMessage: action.errorMessage
            }
    }

    return state;
};