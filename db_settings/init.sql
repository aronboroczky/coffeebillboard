GRANT ALL PRIVILEGES ON DATABASE coffeebillboard TO coffeebillboard_api;
CREATE TABLE IF NOT EXISTS "Coffee"
(
    "CoffeeId" SERIAL PRIMARY KEY,
    "Title" VARCHAR(100) NOT NULL,
    "Price" INTEGER NOT NULL,
    "AvatarLocation" text
);