using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Contracts;
using Entity.Models;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CoffeeBillboardController : ControllerBase
    {
        private readonly ILogger<CoffeeBillboardController> _logger;
        private readonly IRepositoryWrapper _repositoryWraper;

        public CoffeeBillboardController(ILogger<CoffeeBillboardController> logger, IRepositoryWrapper repositoryWrapper)
        {
            _logger = logger;
            _repositoryWraper = repositoryWrapper;
        }

        [HttpGet]
        public IEnumerable<Coffee> Get()
        {
            return _repositoryWraper.Coffees.GetAll();
        }

        [HttpPost]
        public void Post(Coffee coffee)
        {
            _repositoryWraper.Coffees.Add(coffee);
            _repositoryWraper.Complete();
        }

        [HttpDelete("{id}")]
        public void DeleteCoffeFromBillboard(int id)
        {
            var coffeeToRemove = _repositoryWraper.Coffees.Get(id);
            _repositoryWraper.Coffees.Remove(coffeeToRemove);
            _repositoryWraper.Complete();
        }

    }
}