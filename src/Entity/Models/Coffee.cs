using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.Models
{
    [Table("Coffee")]
    public class Coffee
    {
        public int CoffeeId { get; set; }

        [Required(ErrorMessage = "The coffee title isrequired.")]
        public string Title { get; set; } 

        [Required(ErrorMessage = "The price is required.")]
        public int Price { get; set; }

        public string AvatarLocation { get; set; }
    }
}