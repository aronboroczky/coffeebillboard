using Entity.Models;
using System.Collections.Generic;

namespace Contracts
{
    public interface ICoffeeRepository : IRepository<Coffee>
    {
        IEnumerable<Coffee> GeCoffeesPerPage(int pageIndex, int pageSize);
    }
}