import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import { Card, CardBody, CardTitle, CardText, CardImg, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

import * as CoffeesStore from '../store/Coffees';

// At runtime, Redux will merge together...
type CoffeeProps =
    CoffeesStore.CoffeesState // ... state we've requested from the Redux store
    & typeof CoffeesStore.actionCreators // ... plus action creators we've requested
    & RouteComponentProps<{ pageNumber: string }>

class CoffeeList extends React.PureComponent<CoffeeProps> {

    public componentDidMount() {
		this.ensureDataFetched();
	}

	// This method is called when the route parameters change
	public componentDidUpdate() {
		this.ensureDataFetched();
	}

    
    public render() {
        return (
			<React.Fragment>
				<h1 id="coffeeBillboardLabel">The coffees and their prices...</h1>
				<p>....over complications....</p>
				{this.renderCoffeeCards()}
				{this.renderPagination()}
			</React.Fragment>
        );
    }

    private ensureDataFetched() {
		let startPage = parseInt(this.props.match.params.pageNumber, 10) || 0;
		if (startPage < 0) {
			startPage = 0;
		}
		this.props.requestCoffees(startPage);
    }
    
    
	private renderCoffeeCards() {
		return (
			<div className='card-deck' aria-labelledby="coffeeBillboardLabel">
				{this.props.coffees.map((coffee: CoffeesStore.Coffee) =>
					<Card key={coffee.coffeeId} className="card border-light">
						<CardImg top width="100%" src={coffee.avatarLocation} alt={coffee.title} />
						<CardBody>
							<CardTitle>{coffee.title}</CardTitle>
							<CardText>${coffee.price}</CardText>
							<Button color="danger" onClick={() => {this.props.deleteCoffee(coffee.coffeeId)}}>Delete</Button>
						</CardBody>
					</Card>
				)}
			</div>
		);
	}

	private renderPagination() {
		let prevStartCoffeeIndex = (this.props.page || 0) - 1;
		if (prevStartCoffeeIndex < 0) {
			prevStartCoffeeIndex = 0;
		}
		
		let nextStartCoffeeIndex = (this.props.page || 0) + 1;
		if (nextStartCoffeeIndex < 0) {
			nextStartCoffeeIndex = 0;
		}

		return (
			<div className="d-flex justify-content-between pagination">
				<Link className='btn btn-outline-secondary btn-sm' to={`/${prevStartCoffeeIndex}`}>Previous</Link>
				{this.props.isLoading && <span>Loading...</span>}
				<Link className='btn btn-outline-secondary btn-sm' to={`/${nextStartCoffeeIndex}`}>Next</Link>
			</div>
		);
	}
}


export default connect(
	(state: ApplicationState) => state.coffees, // Selects which state properties are merged into the component's props
	CoffeesStore.actionCreators // Selects which action creators are merged into the component's props
)(CoffeeList as any);