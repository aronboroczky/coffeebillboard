import './styles/App.scss';

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Layout from './components/Layout';
import CoffeeList from './components/CoffeeList';
import AddCoffee from './components/AddCoffee';

export default () => (
  <Layout>
    <Switch>
      <Route path={"/:pageNumber?"} exact component={CoffeeList} />
      <Route path={"/add-coffee"} exact component={AddCoffee} />
      <Redirect to={"/"} />
    </Switch>
  </Layout>
);
